from django.urls import path
from receipts.views import AccountListView, ExpenseCategoryListView, ReceiptListView, ReceiptCreateView, AccountCreateView, ExpenseCategoryCreateView

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
    path("accounts/create/", AccountCreateView.as_view(), name="account_new"),
    path("categories/create/", ExpenseCategoryCreateView.as_view(), name="expense_category_create"),
    path("categories/", ExpenseCategoryListView.as_view(), name="expense_category_list"),
    path("accounts/", AccountListView.as_view(), name="account_new")
]
 