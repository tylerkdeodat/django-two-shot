- [x] - for repository from gitlab

- [x] - git clone repo

- [ ] -

* [x] - make accounts app - python manage.py startapp «name»

* [x] - create urls.py file under the accounts app

- [x] - adjust INSTALLED_APPS in settings.py with accounts app
    'accounts.apps.AccountsConfig',

- [x] - import LoginView

- [x] - make path to login/ got to LoginView
    path("login/", LoginView, name="login")

- [x] - in expenses/urls.py "include" the accounts urls.py with path("accounts/")

- [x] - create a templates directory under accounts

- [x] - create a registration directory in templates

- [x] - create login.html in registration

- [x] - <form method="post"> in login.html along w/ fundamental 5

* [x] - in settings.py set LOGIN_REDIRECT_URL = "home"

* [X] - in accounts/urls.py import LogoutView

* [X] - register pathway with path("logout/" ... name=
"logout")

* [X] - create html file for logout called logged_out.html

* [X] - in logged_out.html put in fundamental 5 and put a nice message about being logged out

* [X] - in accounts/views.py import UserCreationForm from django.contrib.auth.forms and login() PSEUDOCODE BELOW

<!-- function signup(request)
    if the request method is "POST", then
        form = UserCreationForm(request.POST)
        if the form is valid, then
            username = the "username" value from the request.POST dictionary
            password = the "password1" value from the request.POST dictionary
            user = create a new user using username and password
            save the user object
            login the user with the request and user object
            return a redirect to the "home" path
    otherwise,
        form = UserCreationForm(request.POST)
    context = dictionary with key "form" and value form
    return render with request, "registration/signup.html", and the context -->

* [X] - create signup.html in templates/registration w/ form post and fundamental 5

* [X] - register signup view in accounts/urls.py with path("signup/",...name="signup")

* [X] - Create django app named "reciepts" using python manage.py startapp «name»

* [x] - install it in settings.py in INSTALLED_APPS

* [X] - create a templates directory for reciepts app

* [X] - create a reciepts directory inside of the templates 

* [X] - create a list.html with <h1>Home</h1>

* [X] - create a class view named ReceiptListView and have it inherit from LoginRequiredMixin and TemplateView 

* [X] - set template_name property to  reciept/list.html

* [X] - register path in receipt/urls.py with path "" and name="home"

* [X] - in expenses/urls.py include reciepts/urls.py w/ path("reciepts/")

* [X] - import RedirectView and register path/ - path("", RedirectView.as_view(url=reverse_lazy("home"))),

* [X] - create base.html inside templates directory but outside of registration or reciepts

* [X] - add links with urls named "logout, login, signup" with the content "Logout, Login, Signup" using link tags and anchor tags

* [X] - create a ExpenseCategory model

* [X] - in Expense Model put in an a name propety max 50 characters

* [X] - an owner property with a foreign key to the user model w/ related_name of "categories" cascade on delete

* [X] - a __str__ method that returns name value

* [X] - Create an Account model w/ name property and max length of 100 characters

* [X] - an owner property that has a foreign key to usermodel related_name of "accounts" and cascade on delete 

* [X] - a str method that returns the name value

* [X] - create a Reciept model 

* [X] -  a vendor property with a max_length of 200 characters

* [X] - a total property that is a DecimalField w/ three decimal places and a max of 10 digits

* [X] - a tax property that is a DecimalField w/ three decimal places and max of 10 digits 

* [X] - a date property that contains a date of when a transaction took place 

* [X] - a purchaser property that is a foreignKey to the User_model with a related_name of "reciepts" and cascade on delete 

* [X] - a category property that is a foreignkey to the "expensecategory" with a related_name of "reciepts"
cascade on_delete, null=true

* [X] - an account property that is a foreighKey to the "Account" model w/ related_name="receipts",
on_delete=models.CASCADE,
null=true

* [X] - create CreateViews for each model

* [X] - path(receipts/accounts/create/) for the view to create a new receipt account

* [X] - path(receipts/categories/create/) for the view to create a new expense category

* [X] - path(receipts/create/) for the view to create a new receipt

* [] - create a list view for each model

* [] - each list view must have 
<!-- def get_queryset(self):
    return Model.objects.filter(user_property=self.request.user) -->